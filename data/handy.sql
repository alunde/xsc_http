DELETE FROM "HTTPODATA"."http.data::http.conn" WHERE "connId" > 2;
DELETE FROM "HTTPODATA"."http.data::http.request" WHERE "reqId" > 2;
DELETE FROM "HTTPODATA"."http.data::http.reqheader" WHERE "hdrId" > 8;
DELETE FROM "HTTPODATA"."http.data::http.reqbody" WHERE "bodyId" > 1;
DELETE FROM "HTTPODATA"."http.data::http.response" WHERE "resId" > 2;
DELETE FROM "HTTPODATA"."http.data::http.resheader" WHERE "hdrId" > 6;
DELETE FROM "HTTPODATA"."http.data::http.resbody" WHERE "bodyId" > 2;
