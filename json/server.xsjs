// Note: This xsjs file can be invoked by the following url:
// http://xsadv.sfphcp.com:8000/http/json/server.xsjs?json={}
// POST with request header Content-Type: application/json

// Write a message to the server side log found in the Administration Perspective on the Default Admin Window Diagnosis Files sub-tab.
// Look for a file of the form xsengine_alert_lt5013.trc with recent modification date.
// Run the script with trace statements in it and immediately refresh the log window.
// You'll have to search for your entires because it's logging for the whole server.

 // import math lib
 //$.import("sap.myapp.lib","math");
 
 // use math lib
 //var max_res = $.sap.myapp.lib.math.max(3, 7);

/* Sample POST request (use in Firefox RESTClient)

{
	//"connId": 0,
	"status": "",
	"rescode": 200,
	"failure": "",
	"proto": "http",
	"user": "user",
	"pass": "pass",
	"host": "xsadv.sfpchp.com",
	"port": 80,
	"path": "/",
	"query": "",
	"frag": "",
	"chost": "0.0.0.0",
	"cport":1080,
	"reqstart": "/Date(" + (new Date()).getTime() +  ")/",
	"reqend": "/Date(" + (new Date()).getTime() +  ")/",
	"resstart": "/Date(" + (new Date()).getTime() +  ")/",
	"resend": "/Date(" + (new Date()).getTime() +  ")/",
	"cconnest": "na",
	"sconninit": "na",
	"sconnhshake": "na",
	"req": {
	    //"reqId": 0,
	    //"parent": 0,
	    "method": "POST",
	    "headers": [
	        { "reqHDR1": "VALUE1" },
	        { "reqHDR2": "VALUE2" },
	        { "reqHDR3": "VALUE3" }
	    ],
	    "body": "{ POST Request Body }"
	},
	"res": {
	    //"resId": 0,
	    //"parent": 0,
	    "headers": [
	        { "resHDR1": "VALUE1" },
	        { "resHDR2": "VALUE2" },
	        { "Content-Type": "text/html" }
	    ],
	    "body": "<html><body>Response Body</body></head>"
	}
}}


 */


//$.trace.debug("request path: " + $.request.getPath());
$.trace.info(":HTTP Server START:");

var STATUS = { 
		"OK":0, 
		"UNHANDLED":1, 
		"NOT_OBJECT":100, 
		"NOT_ARRAY":101, 
		"HR_ERROR":201,
		"CO_ERROR":202,
		"LAST_STATUS":999
		};

var output = "";
var debugging = true;  // Turn on debugging to get session messages.

//Check if an object is empty
function is_empty(obj) {

 // Assume if it has a length property with a non-zero value
 // that that property is correct.
 if (obj.length && (obj.length > 0)) {
     return(false);
 }

 if (obj.length && (obj.length === 0)) {
     return(true);
 }

 var key;
 for (key in obj) {
     if (obj.hasOwnProperty(key)) {
        return(false);
     }
 }

 return(true);
}




var gsid = 0;	//Global Session ID
var gconn = {}; //Global DB gconnection
var guid = 0;	//Global User ID
var gdid = 0;	//Global Device ID
var g_xmit_ts = "";	//Global Xmit TS

function insertConn(conn) {
	var sql = "";

	var connID = 0;

	sql = "select \"HTTPODATA\".\"http.data::connId\".NEXTVAL from DUMMY";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		connID = 0;
	} else {
		connID = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	
	sql = "insert into \"HTTPODATA\".\"http.data::http.conn\" values(";
	
	sql += connID + ",";
	sql += "'" + conn.status + "'"/*status <NVARCHAR(16)>*/ + ",";
	sql +=       conn.rescode + /*rescode <INTEGER>*/",";
	sql += "'" + conn.failure + "'"/*failure <NVARCHAR(255)>*/ + ",";
	sql += "'" + conn.proto + "'"/*proto <NVARCHAR(5)>*/ + ",";
	sql += "'" + conn.user + "'"/*user <NVARCHAR(64)>*/ + ",";
	sql += "'" + conn.pass + "'"/*pass <NVARCHAR(64)>*/ + ",";
	sql += "'" + conn.host + "'"/*host <NVARCHAR(64)>*/ + ",";
	sql +=       conn.port + /*port <INTEGER>*/",";
	sql += "'" + conn.path + "'"/*path <NVARCHAR(128)>*/ + ",";
	sql += "'" + conn.query + "'"/*query <NVARCHAR(128)>*/ + ",";
	sql += "'" + conn.frag + "'"/*frag <NVARCHAR(64)>*/ + ",";
	sql += "'" + conn.chost + "'"/*chost <NVARCHAR(255)>*/ + ",";
	sql +=       conn.cport + /*cport <INTEGER>*/",";
	sql += "'" + conn.reqstart + "'"/*reqstart <TIMESTAMP>*/ + ",";
	sql += "'" + conn.reqend + "'"/*reqend <TIMESTAMP>*/ + ",";
	sql += "'" + conn.resstart + "'"/*resstart <TIMESTAMP>*/ + ",";
	sql += "'" + conn.resend + "'"/*resend <TIMESTAMP>*/ + ",";
	sql += "'" + conn.cconnest + "'"/*cconnest <NVARCHAR(8)>*/ + ",";
	sql += "'" + conn.sconninit + "'"/*sconninit <NVARCHAR(8)>*/ + ",";
	sql += "'" + conn.sconnhshake + "'"/*sconnhshake <NVARCHAR(8)>*/ + "";

	sql += ")";

	var pstmt2 = gconn.prepareStatement(sql);
	pstmt2.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt2.close();
	
	return(connID);
}

function insertRequest(reqObj,connID) {
	var sql = "";

	var reqID = 0;

	sql = "select \"HTTPODATA\".\"http.data::reqId\".NEXTVAL from DUMMY";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		reqID = 0;
	} else {
		reqID = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	sql = "insert into \"HTTPODATA\".\"http.data::http.request\" values(";
	
	sql += reqID + ",";
	sql += connID + /*parent <INTEGER>*/",";
	sql += "'" + reqObj.method + "'"/*method <NVARCHAR(5)>*/ + "";

	sql += ")";

	var pstmt2 = gconn.prepareStatement(sql);
	pstmt2.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt2.close();
	
	return(reqID);
}

function insertReqHdr(hdrObj,reqID) {
	var sql = "";

	var hdrID = 0;

	sql = "select \"HTTPODATA\".\"http.data::reqheaderId\".NEXTVAL from DUMMY";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		hdrID = 0;
	} else {
		hdrID = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	sql = "insert into \"HTTPODATA\".\"http.data::http.reqheader\" values(";
	
	sql += hdrID + ",";
	sql += reqID + /*parent <INTEGER>*/",";
	sql += "'" + hdrObj.name + "'"/*name <NVARCHAR(32)>*/ + ",";
	sql += "'" + hdrObj.value + "'"/*value <NVARCHAR(1028)>*/ + "";

	sql += ")";

	var pstmt2 = gconn.prepareStatement(sql);
	pstmt2.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt2.close();
	
	return(hdrID);
}

function insertReqBody(body,reqID) {
	var sql = "";

	var bodyID = 0;

	sql = "select \"HTTPODATA\".\"http.data::reqbodyId\".NEXTVAL from DUMMY";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		bodyID = 0;
	} else {
		bodyID = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	sql = "insert into \"HTTPODATA\".\"http.data::http.reqbody\" values(";
	
	sql += bodyID + ",";
	sql += reqID + /*parent <INTEGER>*/",";
	sql += "'" + body + "'"/*content <NVARCHAR(1028)>*/ + "";

	sql += ")";

	var pstmt2 = gconn.prepareStatement(sql);
	pstmt2.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt2.close();
	
	return(bodyID);
}

function insertResponse(resObj,connID) {
	var sql = "";

	var resID = 0;

	sql = "select \"HTTPODATA\".\"http.data::resId\".NEXTVAL from DUMMY";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		resID = 0;
	} else {
		resID = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	sql = "insert into \"HTTPODATA\".\"http.data::http.response\" values(";
	
	sql += resID + ",";
	sql += connID + /*parent <INTEGER>*/"";

	sql += ")";

	var pstmt2 = gconn.prepareStatement(sql);
	pstmt2.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt2.close();
	
	return(resID);
}

function insertResHdr(hdrObj,resID) {
	var sql = "";

	var hdrID = 0;

	sql = "select \"HTTPODATA\".\"http.data::resheaderId\".NEXTVAL from DUMMY";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		hdrID = 0;
	} else {
		hdrID = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	sql = "insert into \"HTTPODATA\".\"http.data::http.resheader\" values(";
	
	sql += hdrID + ",";
	sql += resID + /*parent <INTEGER>*/",";
	sql += "'" + hdrObj.name + "'"/*name <NVARCHAR(32)>*/ + ",";
	sql += "'" + hdrObj.value + "'"/*value <NVARCHAR(1028)>*/ + "";

	sql += ")";

	var pstmt2 = gconn.prepareStatement(sql);
	pstmt2.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt2.close();
	
	return(hdrID);
}

function insertResBody(body,resID) {
	var sql = "";

	var bodyID = 0;

	sql = "select \"HTTPODATA\".\"http.data::resbodyId\".NEXTVAL from DUMMY";

	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	if (!rs.next()) {
		bodyID = 0;
	} else {
		bodyID = rs.getInteger(1);
	}

	rs.close();
	pstmt.close();

	sql = "insert into \"HTTPODATA\".\"http.data::http.resbody\" values(";
	
	sql += bodyID + ",";
	sql += resID + /*parent <INTEGER>*/",";
	sql += "'" + body + "'"/*content <NVARCHAR(1028)>*/ + "";

	sql += ")";

	var pstmt2 = gconn.prepareStatement(sql);
	pstmt2.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt2.close();
	
	return(bodyID);
}


//Doesn't work with JSON.parse (object member names must be double quoted.)
//var strJSON = '{cmd:"HRDATA",data:[{chr:78},{chr:79},{chr:82}]}';

//Works with JSON.parse
var strJSON = '{"cmd":"HRDATA","data":[{"chr":78},{"chr":79},{"chr":82}]}';


try {
	// Always assume and exception can occur

	var i=0;
	
	var request_method = $.request.method;
	var method = "";
	
	method += $.request.toString();

	switch(request_method) {
	case $.net.http.GET:	// GET == 1
		method = "GET";
		//var jsonParam = $.request.getParameter("json"); // Deprecated syntax doesn't work!
		strJSON = "";
		// Parameters from the HTTP GET query string
		for (i=0; i<$.request.parameters.length; ++i) {
		    var name = $.request.parameters[i].name;
		    var value = $.request.parameters[i].value;
		    if (name === "json") {
				strJSON = value;
				// method += "json: " + value;
		    }
		    else {
				method += " only param named json accepted. not " + name;
		    }
		}
		if (strJSON !== "") {
			method += " " + strJSON;
		}
		else {
			method += " empty param named json.";
		}
		break;
	case 2: // Unknown
		method = "Unknown";
		break;
	case $.net.http.POST: // POST = 3
		method = "POST";
		
		// Loop through all the HTTP headers and only process the body if content-type is application/json
		var content_type = "";
		for (i=0; i<$.request.headers.length; ++i) {
		    var name = $.request.headers[i].name;
		    var value = $.request.headers[i].value;

		    //method += " header[" + i + "] = " + name + ":" + value + "\n";

		    if (name === "content-type") {
				content_type = value;
			    method += " header[" + i + "] = " + name + ":" + value + "\n";
		    }
 
		}
		if (content_type.indexOf("application/json") !== -1) {
			// Extract the body of the POST
			strJSON = $.request.body.asString();
			//method += " POSTED BODY: " + strJSON;
		}
		else {
			method += "\n ERROR: expected content-type:applicaton/json.";
		}
		
		break;
	case $.net.http.PUT: // PUT = 4
		method = "PUT";
		break;
	case $.net.http.DEL: // DELETE = 5
		method = "DELETE";
		break;
	case 0: // OPTIONS
		method = "OPTIONS";
		break;
	case $.net.http.gconnECT: // gconnECT = 7
		method = "gconnECT";
		break;
	default:
		method = "Unhandled";
		break;
	}

	output = "HTTP JSON Server : " + method + "\n\n";

	gconn = $.db.getConnection();
	
	// Parse the JSON string into an object
//	var objJSON = JSON.parse(strJSON);


var objJSON = 
{
	//"connId": 0,
	"status": "",
	"rescode": 200,
	"failure": "",
	"proto": "http",
	"user": "user",
	"pass": "pass",
	"host": "xsadv.sfpchp.com",
	"port": 80,
	"path": "/",
	"query": "",
	"frag": "",
	"chost": "0.0.0.0",
	"cport":1080,
	"reqstart": new Date().toISOString(),
	"reqend": new Date().toISOString(),
	"resstart": new Date().toISOString(),
	"resend": new Date().toISOString(),
	"cconnest": "na",
	"sconninit": "na",
	"sconnhshake": "na",
	"req": {
	    //"reqId": 0,
	    //"parent": 0,
	    "method": "POST",
	    "headers": [
	        { "name": "reqHDR1", "value": "VALUE1" },
	        { "name": "reqHDR2", "value": "VALUE2" },
	        { "name": "reqHDR3", "value": "VALUE3" }
	    ],
	    "body": "{ POST Request Body }"
	},
	"res": {
	    //"resId": 0,
	    //"parent": 0,
	    "headers": [
	        { "name": "resHDR1", "value": "VALUE1" },
	        { "name": "Content-Type", "value": "text/html" }
	    ],
	    "body": "<html><body>Response Body</body></head>"
	}
};


    if (strJSON !== "{}") {
        objJSON = JSON.parse(strJSON);
    }

	//alert("JSON: \n\n" + strJSON);

	output += "strJSON = " + strJSON + "\n";

	//output += "objJSON = " + objJSON + "\n";
	
	output += "\n";	

	if (objJSON instanceof Object) {
		
		// Save the connection data
		var connID = insertConn(objJSON);
		
		var req = objJSON.req;
		var res = objJSON.res;
		
		if (req instanceof Object) {
		    
		    // Save the request data
		    var reqID = insertRequest(req,connID);

		    var reqHeaders = req.headers
		    
			for (i=0; i<reqHeaders.length; i++) {
			    var hdr = reqHeaders[i];
			    // Save each request header
			    insertReqHdr(hdr,reqID);
			}
			
			if (req.body !== null) {
			    // Save the request body
			    insertReqBody(req.body,reqID)
			}
		}
		else if (req instanceof Array) {
			output += "Error:" + " Expected an object not an array.";
		}
		else {
			output += "Not a handled data object";
		}
		
		if (res instanceof Object) {
		    
		    // Save the request data
		    var resID = insertResponse(res,connID);

		    var resHeaders = res.headers
		    
			for (i=0; i<resHeaders.length; i++) {
			    var hdr = resHeaders[i];
			    // Save each request header
			    insertResHdr(hdr,resID);
			}
			
			if (res.body !== null) {
			    // Save the request body
			    insertResBody(res.body,resID)
			}
		}
		else if (res instanceof Array) {
			output += "Error:" + " Expected an object not an array.";
		}
		else {
			output += "Not a handled data object";
		}

	}
	else {
		// Expected top level JSON element to be an object(containing an array of cmd objects)
		output += "Error:" + " Expected an object at the top level.";
	}
	

	output += "\nFINISHED... \n";
	
	$.response.contentType = "text/html";
	$.response.setBody(output);


} catch (exception) {
	// Figure out what kind of exception it is and handle ones we care about.
	if (exception instanceof TypeError) {
		// Handle TypeError exception
		output += "TypeError: " + exception.toString() + "\n";
	} else if (exception instanceof ReferenceError) {
		// Handle ReferenceError
		output += "ReferenceError: " + exception.toString() + "\n";
	} else if (exception instanceof SyntaxError) {
		// Handle SyntaxError
		output += "SyntaxError: " + exception.toString() + "\n";
	} else {
		// Handle all other not handled exceptions
		output += "ExceptionError: " + exception.toString() + "\n";
	}
	
	$.response.contentType = "text/html";
	$.response.setBody(output);
	
} finally {
	// This code is always executed even if an exception is 
	if (gconn instanceof Object) {
		gconn.close();
	}
}

$.trace.info(":HTTP Server END:");


